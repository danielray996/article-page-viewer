import React, { Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import { createArticle } from '../../models/Article';
import { createPage } from '../../models/Page';
import Articles from '../Articles';
import Page from '../Page';

class ContentMultiPurpose extends React.Component {
    constructor() {
        super();

        this.state = {
            page: {},
            articles: [{
                id: 0,
                categoryID: 0
            }],
            isLoading: true
        }
    }
    
    async componentDidUpdate(prevProps, prevState) {
        try {
            const type =  this.props.match.params.type;
            const contentUpdate = await this.callTypeData();
            const matchPage = type === 'page' && prevState.page.id !== contentUpdate.id;
            const matchArticles = type === 'category' && prevState.articles[0].categoryID !== contentUpdate[0].categories[0];

            if (matchPage || matchArticles) {
                this.controlContent(contentUpdate);
            }
        } catch(err) {
            console.log(err);
        }
    }

    async componentDidMount() {
        const data = await this.callTypeData();
        await this.controlContent(data);
        setTimeout(() => {
            this.setState({
                isLoading: false
            })
        }, 1000)
    }

    async callTypeData() {
        const controlType = this.props.match.params.type === 'page' ? `pages/${this.props.match.params.id}` : `posts?categories=${this.props.match.params.id}`;
        let content = await fetch(`${process.env.REACT_APP_API_URL}/${controlType}`).then(res => res.json());
        return content;
    }

    async controlContent(updateElement) {
        try {
            if(this.props.match.params.type === 'page'){
                this.setState({
                    page: createPage(updateElement)
                });
            } else {
                this.setState({
                    articles: updateElement.map(article => createArticle(article))
                })
            }
        } catch(e) {
            console.log(e);
        }
    }

    render() {
        return(
            <Fragment>
                { !this.state.isLoading &&
                    <div id="container">
                        { 
                            this.props.match.params.type !== 'category' && 
                            <Page page={ this.state.page }/>
                        }
                        { 
                            this.props.match.params.type !== 'page' &&
                            <Articles name={`Articoli della categoria ${this.props.match.params.slug.toUpperCase()}`} articles={this.state.articles}/>
                        }
                    </div>
                }
                {
                    this.state.isLoading &&
                    <div>
                        <h1>Sto caricando, aspetta!</h1>
                    </div>
                }
            </Fragment>
        );
    }
}

export default withRouter(ContentMultiPurpose);