import React from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronCircleDown, faNewspaper } from '@fortawesome/free-solid-svg-icons'
import Logo from '../Logo';
import NavLink from '../NavLink';
import { createCategory } from '../../models/Category';
import { createPage } from '../../models/Page';

export default class Navbar extends React.Component {
    constructor() {
        super();

        this.state = {
            categories: [],
            pages: []
        }
    }

    async componentDidMount() {
        const categories = await fetch(`${process.env.REACT_APP_API_URL}/categories`).then(res => res.json());
        const pages = await fetch(`${process.env.REACT_APP_API_URL}/pages`).then(res => res.json());

        this.setState({
            categories: categories.map(cat => createCategory(cat)),
            pages: pages.map(page => createPage(page))
        })
    }

    render() {
        const categories = this.state.categories.map(category => 
            <NavLink key={category.id} 
            id={category.id} 
            name={category.name} 
            slug={category.slug} 
            type={category.type}/> 
        );
        const pages = this.state.pages.map(page=> 
            <NavLink key={page.id} 
            id={page.id} 
            name={page.title} 
            slug={page.slug} 
            type={page.type}/> 
        );

        return(
            <nav className="bg-dark navsize">
                <Logo/>
                <ul className="navbar-nav me-auto mb-2 mb-lg-0 item-sliding-right">
                    <li>
                        <p className="text-white border-bottom">
                            <FontAwesomeIcon icon={faChevronCircleDown}/> Menu
                        </p>
                    </li>
                    <li className="nav-item border-bottom">
                        <Link className="font-nav" to="/">
                            <FontAwesomeIcon icon={faNewspaper}/> Home
                        </Link>
                    </li>
                    { pages }
                    <li>
                        <p className="text-white border-bottom mt-3">
                            <FontAwesomeIcon icon={faChevronCircleDown}/> Categories
                        </p>
                    </li>
                    { categories }
                </ul>
            </nav>
        );
    }
}