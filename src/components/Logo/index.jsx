import React from 'react';
import reactLogo from '../../assets/logo/react.svg';
import wordpressLogo from '../../assets/logo/wordpress.svg';

export default class Logo extends React.Component {

    render(){
        return(
            <div className="mb-5">
                <img src={reactLogo} alt="logo" className="logo position-absolute near-pos item-sliding-right"/>
                <img src={wordpressLogo} alt="logo" className="logo item-sliding-up"/>
            </div>
        );
    }
}