import React from 'react';
export default class Footer extends React.Component {
    render() {
        return(
            <footer className="container-fluid border-top item-sliding-up">
                <div className="container d-flex flex-wrap justify-content-between align-items-center py-3 my-4">
                    <div className="col-md-4 d-flex align-items-center">
                        <span className="text-muted"><strong>Ray Soft</strong> © 2021 Company, Inc</span>
                    </div>
                </div>
            </footer>
        );
    }
}