import React from 'react';
import { withRouter } from 'react-router';
import { createArticle } from '../../models/Article';
import Page from '../Page';

class ArticleView extends React.Component {
    constructor() {
        super();

        this.state = {
            article: {}
        }
    }

    async componentDidMount() {
        const article = await fetch(`http://localhost/bedrock/web/wp-json/wp/v2/posts/${this.props.match.params.id}`).then(res => res.json());
        this.setState({
            article: createArticle(article)
        })
    }

    render() {
        return(
            <Page page={this.state.article}/>
        );
    }
}

export default withRouter(ArticleView);