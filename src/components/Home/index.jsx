import React from 'react';
import { createArticle } from '../../models/Article';
import Articles from '../Articles';

export default class Home extends React.Component {
    constructor() {
        super();

        this.state = {
            articles: []
        }
    }

    async componentDidMount() {
        const articles = await fetch(`${process.env.REACT_APP_API_URL}/posts`)
        .then(res => res.json())
        this.setState({
            articles: articles.map(article => createArticle(article))
        })
    }

    render() {
        return(
            <Articles name={'Home'} articles={this.state.articles}/>
        );
    }
}