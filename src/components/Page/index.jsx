import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Markup } from 'interweave';
import React from 'react';

export default class Page extends React.Component {

    render() {
        return(
            <div className="p-5 item-sliding-up">
                <FontAwesomeIcon icon={faPaperPlane}/><h1 className="mb-5" dangerouslySetInnerHTML={{__html: this.props.page.title}}/>
                <div className="mb-5 p-3 border border-2 rounded content">
                  <Markup content={ this.props.page.content } />
                </div>
                <p className="mt-4 text-end">
                    <strong>Pubblicato: </strong> il { this.props.page.date } alle ore { this.props.page.hour }
                </p>
            </div>
        );
    }
}