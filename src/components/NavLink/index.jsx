import React from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as iconBrands from '@fortawesome/free-brands-svg-icons';

export default class NavLink extends React.Component {

    iconGenerate = (icon) => {
        let firstLetter = icon.slice(0, 1).toUpperCase();
        let remainWord = icon.slice(1, icon.length);
        let fullWordIcon = firstLetter + remainWord;
        if(!fullWordIcon.includes('-') && iconBrands[`fa${fullWordIcon}`] !== undefined){
            return `fa${fullWordIcon}`;
        } else {
            return 'faBandcamp';
        }
    }

    render() {
        return(
            <li className="nav-item border-bottom">
                <Link className="font-nav" to={`/${this.props.type}/${this.props.slug}/${this.props.id}`}>
                    <FontAwesomeIcon icon={iconBrands[this.iconGenerate(this.props.slug)]}/> { this.props.name }
                </Link>
            </li>
        );
    }
}