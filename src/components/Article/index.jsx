import { Markup } from 'interweave';
import React from 'react';
import { Link } from 'react-router-dom';
import notFound from '../../assets/images/not-found.jpg';

export default class Article extends React.Component {
    constructor() {
        super();

        this.state = {
            image: ''
        }
    }

    async componentDidMount() {
        if(this.props.article.imageID !== undefined && this.props.article.imageID !== 0){
            try {
                const image = await fetch(`${process.env.REACT_APP_API_URL}/media/${this.props.article.imageID}`)
                .then(res => res.json())
                this.setState({
                    image: image.source_url
                })
            } catch (err) {
                console.log(err);
            }
        } else {
            this.setState({
                image: notFound
            })
        }
    }

    render() {
        return(
            <div className="col-3 m-2">
                <div className="card me-1">
                    <img src={this.state.image} className="card-img-top" alt="Copertina"/>
                    <div className="card-body text-center">
                        <h5 className="card-title">
                            <Markup content={this.props.article.title}/>
                        </h5>
                        <Markup content={this.props.article.description}/>
                    </div>
                    <div className="text-center mb-4">
                        <Link to={`/${this.props.article.slug}/${this.props.article.id}`} className="button fw-bold">...Leggi di più</Link>
                    </div>
                    <div className="card-footer text-end">
                        <small>Published <Markup content={this.props.article.date}/> at <Markup content={this.props.article.hour}/></small>
                    </div>
                </div>
            </div>
        );
    }
}

// export default withRouter(Article);