import { Markup } from 'interweave';
import React from 'react';
import Article from '../Article';

export default class Articles extends React.Component {
    render() {
        return(
            <div id={`${this.props.name === 'Home' ? 'home' : 'article'}`} style={{"height": "100%"}}>
                <h1 className={`display-5 text-center pt-3 text-shadow item-sliding-up text-white`}>
                    <Markup content={this.props.name}/>
                </h1>
                <div className="card-group p-4 justify-content-center item-sliding-right">
                    { this.props.articles.map(article => <Article key={article.id} article={article}/> ) }
                </div>
            </div>
        );
    }
}