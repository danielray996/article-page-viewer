export class Category {
    constructor(id, name, slug, type){
        this.id = id;
        this.name = name;
        this.slug = slug;
        this.type = type;
    }
}

export function createCategory(category){
    return new Category(category.id, category.name, category.slug, category.taxonomy);
}