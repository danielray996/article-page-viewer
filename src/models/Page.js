export class Page {
    constructor(id, title, content, slug, date, hour, type){
        this.id = id;
        this.title = title;
        this.content = content;
        this.slug = slug;
        this.date = date;
        this.hour = hour;
        this.type = type;
    }
}

export function createPage(page){
    const data = page.date_gmt.split('T');
    return new Page(page.id, page.title.rendered, page.content.rendered, page.slug, data[0], data[1], page.type);
}