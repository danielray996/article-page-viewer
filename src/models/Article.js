export class Article {
    constructor(id, title, description, content, date, hour, link, imageID, slug, categoryID){
        this.id = id;
        this.title = title;
        this.description = description;
        this.content = content;
        this.date = date;
        this.hour = hour;
        this.link = link;
        this.imageID = imageID;
        this.slug = slug;
        this.categoryID = categoryID;
    }
}

export function createArticle(article){
    const data = article.date_gmt.split('T');
    let description = article.excerpt.rendered.split('<a');
    description[0] = description[0] + '</p>\n';
    return new Article(article.id, article.title.rendered, description[0], article.content.rendered, data[0], data[1], article.link, article.featured_media, article.slug, article.categories[0]);
}