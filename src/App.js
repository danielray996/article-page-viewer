import { BrowserRouter } from 'react-router-dom';
import { Switch, Route } from 'react-router-dom';
import Footer from './components/Footer';
import Navbar from './components/Navbar';
import ContentMultiPurpose from './components/ContentMultiPurpose';
import Home from './components/Home';
import ArticleView from './components/ArticleView';

function App() {
  return (
    <BrowserRouter>
      <div className="d-flex flex-row">
        <Navbar/>
        <Switch>
          <Route path="/" exact>
            <Home/>
          </Route>
          <Route path="/:type/:slug/:id">
            <ContentMultiPurpose/>
          </Route>
          <Route path="/:slug/:id">
            <ArticleView/>
          </Route>
        </Switch>
      </div>
      <Footer/>
    </BrowserRouter>
  );
}

export default App;
